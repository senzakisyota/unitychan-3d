using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySc : MonoBehaviour
{
    GameObject player;
    float Aspeed;
    GameObject ScoreT;
    public GameObject Heff;
    //GameObject Ge;
   
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("unitychan");  //unitychanを取得
        ScoreT = GameObject.Find("ScoreT");    //scoreTを取得
        //Ge = GameObject.Find("EneGe");
        int RanS = Random.Range(1, 4);        //1-3の整数値をランダムで取得
        if (RanS == 1)                    //1のとき
        {
            Aspeed = 0.1f;
        }
        if (RanS == 2)　　　　　　　　　　//2のとき
        {
            Aspeed = 0.08f;
        }
        if (RanS == 3)                    //3のとき
        {
            Aspeed = 0.12f;
        }
    }
   
    private void OnTriggerEnter(Collider other)               //playerと接触したら
    {
        if (other.gameObject.tag == "player")　　　　　　　　//playerタグと接触した時に呼ばれる
        {
             ScoreT.GetComponent<scorecount>().Plife();     //ライフ減算
           
            Destroy(gameObject);                           //消滅
            Instantiate(Heff, this.transform.position, Quaternion.identity);     //エフェクトの発生

        }
       // Destroy(gameObject);
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        
        

        transform.Translate(Aspeed, 0, 0);          //速度の代入
        //Aspeed *= 1.05f;
        if (transform.position.x > 20.0f)          //x＞20で消滅
        {
            Destroy(gameObject);
            
        }
       
       
    
    }
    
}

