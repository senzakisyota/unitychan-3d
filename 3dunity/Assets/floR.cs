using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floR : MonoBehaviour
{
    float UPR = 0.005f;      //下がる速度
    float TRR = 0.005f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(0, -UPR, 0);
        if (transform.position.y < -0.5f)    //位置指定
        {
            UPR = 0;
            transform.Translate(-TRR, 0, 0);
            if (transform.position.x < -29)
            {
                TRR = 0;
            }
        }
    }
}
