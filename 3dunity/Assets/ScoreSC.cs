using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSC : MonoBehaviour
{
    GameObject player;
    float Aspeed = 0.2f;
    GameObject ScoreT;
    public GameObject Eff;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("unitychan"); 
        ScoreT = GameObject.Find("ScoreT");

        int RanS2 = Random.Range(1, 4);  
        if (RanS2 == 1)
        {
            Aspeed = 0.12f;
        }
        if (RanS2 == 2)
        {
            Aspeed = 0.08f;
        }
        if (RanS2 == 3)
        {
            Aspeed = 0.1f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "player")
        {
            ScoreT.GetComponent<scorecount>().Addscore();
            Destroy(gameObject);
            Instantiate(Eff, this.transform.position, Quaternion.identity);
        }
        // Destroy(gameObject);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(Aspeed, 0, 0);
        //Aspeed *= 1.05f;
        if (transform.position.x > 20.0f)
        {
            Destroy(gameObject);

        }
    }

}
