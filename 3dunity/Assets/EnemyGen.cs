using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGen : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public GameObject croqiette_t;
    float span = 1.0f;       //生成速度の設定
    float delta = 0;
    public GameObject EnPa;
   
    //int Rnd = Random.Range(1, 3);
    void Start()
    {
      
    }

    // Update is called once per frame
    public void FixedUpdate()                 //環境で速度に差異が生じるためFixed
    {
         int Rnd = Random.Range(0, 9);   //0-8の整数値をランダムで出力する　0は何も起こさない
        if (Time.time >= 13.5f)            //約13秒後から出力
        {
            delta += Time.deltaTime; //deltaに加算
            if (delta > span) //上で設定した速度で計算
            {
                delta = 0; //リセット


                if (Rnd == 1)          //1のとき〜以下同文
                {
                    //Debug.Log("1");
                    Instantiate(EnemyPrefab, new Vector3(-14.5f, 0.5f, 7.9f), Quaternion.identity); // 位置指定
                    Instantiate(EnPa, this.transform.position, Quaternion.identity);               //エフェクトの発生
                }
                if (Rnd == 2)
                {
                    //Debug.Log("2");
                    Instantiate(EnemyPrefab, new Vector3(-14.5f, 0.5f, 7.9f), Quaternion.identity);
                    //EnemyPrefab.GetComponent<EnemySc>().Speedch();
                    Instantiate(EnPa, this.transform.position, Quaternion.identity);
                }
                if (Rnd == 3)
                {
                    //Debug.Log("3");
                    Instantiate(EnemyPrefab, new Vector3(-14.5f, 3.0f, 7.9f), Quaternion.identity);
                    Instantiate(EnPa, this.transform.position, Quaternion.identity);

                }
                if (Rnd == 4)
                {
                    //Debug.Log("4");
                    Instantiate(EnemyPrefab, new Vector3(-14.5f, 3.0f, 7.9f), Quaternion.identity);
                    // EnemyPrefab.GetComponent<EnemySc>().speedch();
                    Instantiate(EnPa, this.transform.position, Quaternion.identity);
                }
                if (Rnd == 5)
                {
                   // Debug.Log("5");
                    Instantiate(croqiette_t, new Vector3(-14.5f, 0.5f, 7.9f), Quaternion.identity);
                    Instantiate(EnPa, this.transform.position, Quaternion.identity);
                }
                if (Rnd == 6)
                {
                    //Debug.Log("6");
                    Instantiate(EnPa, this.transform.position, Quaternion.identity);
                    Instantiate(croqiette_t, new Vector3(-14.5f, 0.5f, 7.9f), Quaternion.identity);
                }
                if (Rnd == 7)
                {
                   // Debug.Log("7");
                    Instantiate(croqiette_t, new Vector3(-14.5f, 3.0f, 7.9f), Quaternion.identity);
                    Instantiate(EnPa, this.transform.position, Quaternion.identity);
                }
                if (Rnd == 8)
                {
                   // Debug.Log("8");
                    Instantiate(EnPa, this.transform.position, Quaternion.identity);
                    Instantiate(croqiette_t, new Vector3(-14.5f, 3.0f, 7.9f), Quaternion.identity);
                }
            }

        }

    }

}


