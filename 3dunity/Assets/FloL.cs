using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloL : MonoBehaviour
{
    float UPL = 0.005f;     //下がる速度
    float TRL = 0.005f;     //横に行く速度
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(0, -UPL, 0);
        if (transform.position.y < -0.5f)           //位置指定
        {
            UPL = 0;
            transform.Translate(TRL, 0, 0);
            if (transform.position.x > -11)
            {
                TRL = 0;
            }
        }
    }
}
