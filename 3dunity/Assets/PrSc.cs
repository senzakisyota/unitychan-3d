using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrSc : MonoBehaviour
{
    float speed = 0.22f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(0, 0, speed);
        speed*=1.1f;

    }
}
