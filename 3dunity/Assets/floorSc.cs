using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floorSc : MonoBehaviour
{
    float UPS = 0.005f; //床の上昇速度の設定
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(0, UPS, 0);　//上昇
        if (transform.position.y > 0)   //yが0より大きくなったら停止　==にしないのはたまにフレーム抜けで上昇し続けるため
        {
            UPS *= 0;
        }
    }
}
