using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scorecount : MonoBehaviour
{
    int score = 0;
    int life = 3;   //ライフ3
    public Text scoretext;
    GameObject life1;
    GameObject life2;
    GameObject life3;
    GameObject player;
    //public GameObject DeEf;
    // Start is called before the first frame update
    public void Addscore()
    {
        score++;        //コロッケをとったら呼ばれてスコア加算
    }

    public void Plife()
    {
        life--;      //大砲に当たったらライフをマイナス
    }
    void Start()
    {
         life1 = GameObject.Find("life1");      //ライフ消滅
         life2 = GameObject.Find("life2");
         life3 = GameObject.Find("life3");
        player = GameObject.Find("unitychan");   
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(score);
        scoretext.text = "×" + score.ToString();         //scoreの表示
        //Debug.Log(life);
        if (life == 2)
        {
            Destroy(life3);
        }
        if (life == 1)
        {
            Destroy(life2);
        }
        if (life <= 0)
        {
            Destroy(life1);
            Destroy(player);
            //Instantiate(DeEf, this.transform.position, Quaternion.identity);
        }
    }
}
